package Task

import "fmt"

func Task11DecToBin()  {
	var decNum int
	//---------------------
	fmt.Println("Task 11")
	fmt.Println("Enter number")
	fmt.Scanf("%d", &decNum)
	fmt.Println("Dec: ", decNum)
	fmt.Println("Bin: ", decToBin(decNum))
	//fmt.Printf("Bin: %b", decNum) //EasyWay
}

//DEC TO BIN
func decToBin(number int) string {
	var str string
	for number != 0 {
		if number % 2 == 0 {
			number /= 2
			str += "0"
		} else {
			number = (number - 1) / 2
			str += "1"
		}
	}
	str = reverse(str)
	return str
}