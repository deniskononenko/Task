package Task

import "fmt"
import (
	"math/rand"
	"time"

)

const Size = 11

func Task12BattleShips() int{
	var Field [Size][Size]int
	for i:=0; i<Size; i++{
		for j:=0; j<Size; j++{
			if i == 0 {
				Field[i][j] = j
			}
			if j == 0 {
				Field[i][j] = i
			}
			fmt.Print(Field[i][j], "   ")
		}
		fmt.Println("")
		x,y := GenerateShipsPos()
		for i:=1; i<5; i++ {
			PlaceShip(x, y, i, Field)
		}
	}
	return 0
}

func GenerateShipsPos() (x,y int)  {
	rand.Seed(time.Now().UnixNano())

	x = rand.Int()%(Size-1) + 1
	y = rand.Int()%10 + 1
	return
}

func PlaceShip(x,y,shipSize int, Field [Size][Size]int) [Size][Size]int {
	var check bool
	for !check{
	if x + shipSize > Size || y + shipSize > Size {
		x, y = GenerateShipsPos()
		check = false
	} else {
		check = true
		for i:=x; i<=x+shipSize; i++ {
			Field[i][shipSize] = shipSize
		}
	}

	}
	return Field
}




