package Task

import "fmt"

const lenOfArr = 4
func Task2SumMul () int {
	array := [lenOfArr]int {1,2,3,4}
	//---------------------
	fmt.Println("Task 2")
	fmt.Println("Sum of elements = ", sum(array));
	fmt.Println("Multiply of elements = ", multiply(array));
	fmt.Println("-----------------------")
	return 0
}

//SUM
func sum(arr [lenOfArr]int) int {
	var sum = 0
	for i:=0; i<lenOfArr; i++{
		sum += arr[i]
	}
	return sum
}

//MULTIPLY
func multiply(arr [lenOfArr]int) int {
	var mul = 1
	for i:=0; i<lenOfArr; i++{
		mul *= arr[i]
	}
	return mul
}

