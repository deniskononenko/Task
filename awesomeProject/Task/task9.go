package Task

import "fmt"

func Task9Brackets() int {
	var strBrackets string
	//---------------------
	fmt.Println("Task 9")
	fmt.Println("Enter string: ")
	fmt.Scanf("%s", &strBrackets)
	fmt.Println("Brackets are balanced: ", brackets(strBrackets))
	fmt.Println("-----------------------")
	return 0
}

//BRACKETS
func brackets(str string) bool{
	var brackets = 0
	for i:=0; i<len(str); i++ {
		if string(str[i]) == "["{
			brackets++
		}
		if string(str[i]) == "]" {
			brackets--
		}
		if brackets < 0 {
			return false
		}
	}
	if brackets == 0 {
		return true
	} else {
		return false
	}
}
