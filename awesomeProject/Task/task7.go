package Task

import "fmt"

const lenOfDiagRevMatrix = 3

func Task7DiagonalReverse() int  {
	arrayDiagRev := [lenOfDiagRevMatrix][lenOfDiagRevMatrix]int {{1,2,3},{4,5,6},{7,8,9}}
	//---------------------
	fmt.Println("Task 7")
	fmt.Println(arrayDiagRev)
	fmt.Println(diagRev(arrayDiagRev))
	fmt.Println("-----------------------")
	return 0
}

//DIAGONAL REVERSE
func diagRev(arr [lenOfDiagRevMatrix][lenOfDiagRevMatrix]int) [lenOfDiagRevMatrix][lenOfDiagRevMatrix]int{
	var arrFin [lenOfDiagRevMatrix][lenOfDiagRevMatrix]int

	for i:=0; i<lenOfDiagRevMatrix; i++ {
		for j:=0; j<lenOfDiagRevMatrix; j++  {
			arrFin[i][j] = arr[j][i]
		}
	}
	return arrFin
}
