package Task

import "fmt"

const lenOfArrHist = 4
func Task5Histogram() int  {
	arrayHist := [lenOfArrHist]int {3,5,2,5}
	//---------------------
	fmt.Println("Task 5")
	histogram(arrayHist)
	fmt.Println("-----------------------")
	return 0
}

//HISTOGRAM
func histogram(arr [lenOfArrHist]int) int {
	var str string
	for i:=0; i<lenOfArrHist; i++ {
		str = ""
		for j:=0; j<arr[i]; j++ {
			str += "*"
		}
		fmt.Println(str)
	}
	return 0
}