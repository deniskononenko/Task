package Task

import "fmt"

func Task4Palindrome() int {
	var palindrStr string
	//---------------------
	fmt.Println("Task 4")
	fmt.Println("Enter string: ")
	fmt.Scanf("%s", &palindrStr)
	fmt.Println("Palindrome: ", isPalindrome(palindrStr))
	fmt.Println("-----------------------")
	return 0
}

//PALINDROME
func isPalindrome(str string) bool {
	var strRev = reverse(str)
	if str == strRev {
		return true
	}	else {
		return false
	}
}
