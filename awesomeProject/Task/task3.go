package Task

import "fmt"

func Task3ReverseSring() int {
	var stringDirect string
	//---------------------
	fmt.Println("Task 3")
	fmt.Println("Enter string: ")
	fmt.Scanf("%s", &stringDirect)
	fmt.Println("Direct String: ", stringDirect)
	fmt.Println("Reverse string: ", reverse(stringDirect))
	fmt.Println("-----------------------")
	return 0
}

// REVERSE
func reverse(str string) string{
	var revStr string
	var char string
	for i:= len(str)-1; i>=0; i-- {
		char = string(str[i])
		revStr += char
	}
	return revStr
}


