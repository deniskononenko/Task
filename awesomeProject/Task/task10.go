package Task

import (
	"fmt"
	"strings"
)

func Task10Frequency() int {
	var strFreq string
	//---------------------
	fmt.Println("Task 10")
	fmt.Println("Enter string: ")
	fmt.Scanf("%s", &strFreq)
	charFreq(strFreq)
	fmt.Println("-----------------------")
	return 0
}

//CHAR FREQUENCY
func charFreq(str string) int {
	var charArr string
	var freqArr [100]int
	var count int
	for i:=0; i<len(str); i++{
		count = 1
		charArr += string(str[i])
		for j:=i+1; j<len(str); j++ {
			if string(str[i]) == string(str[j]) {
				count++
			}
		}
		str = strings.Replace(str, string(str[i])," ", -1)
		freqArr[i] = count
	}
	for i:=0; i<len(charArr); i++{
		if string(charArr[i]) != " " {
			fmt.Println("Frequency of char: ", string(charArr[i]), freqArr[i])
		}
	}
	return 0
}
