package Task

import "fmt"
import "math/rand"

func Task8Game() int {
	var tryNumber = 3
	//---------------------
	fmt.Println("Task 8")
	game(tryNumber)
	fmt.Println("-----------------------")
	return 0
}

//GAME
func game(tryNumb int) int {

	var rangeOfGame int
	var number int
	var try int

	fmt.Print("Enter range of game: ")
	fmt.Scanf("%d", &rangeOfGame)

	number = rand.Int()%rangeOfGame

	for i:=0; i<tryNumb; i++ {

		fmt.Print("Enter your number: ")
		fmt.Scanf("%d", &try)

		if number == try {
			fmt.Println("Congrats!!!")
			return 0
		} else {
			fmt.Println("Wrong!!!")
		}
	}
	fmt.Println("Number was: ", number)
	return 0
}
