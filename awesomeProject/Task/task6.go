package Task

import (
	"fmt"
	"strings"
)

func Task6Caesar() int{
	var caesarStr string
	var caesarKey int
	//---------------------
	fmt.Println("Task 6")
	fmt.Println("Enter string: ")
	fmt.Scanf("%s", &caesarStr)
	fmt.Println("Enter key: ")
	fmt.Scanf("%d", caesarKey)
	fmt.Println("Decrypted string: ", caesarStr)
	fmt.Println("Encrypted string: ", caesar(caesarStr, caesarKey))
	fmt.Println("-----------------------")
	return 0
}

//CAESAR
func caesar(str string, key int) string{
	var strFin string
	str = strings.ToLower(str)
	for i:=0; i<len(str); i++ {
		strFin += string(((int((str[i])-97) + key)%26)+97)
	}
	return strFin
}
